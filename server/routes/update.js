// Add all the update routes here
// logic (call back function) for the update request is in controller/update.js
import express from 'express';

import { updatePerson, 
        updateVehicle, 
        updateVehicleTrip, 
        updateFlightTrip,
        updateOrders 
        } from '../controllers/update.js';

const routerUpdate = express.Router();

//TODO add authJwt.js to all calls that should be verified (check ./middleware)

routerUpdate.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

routerUpdate.post('/updatePerson', updatePerson);

routerUpdate.post('/updateVehicle', updateVehicle);

routerUpdate.post('/updateVehicleTrip', updateVehicleTrip);

routerUpdate.post('/updateFlightTrip', updateFlightTrip);

routerUpdate.post('/updateOrders', updateOrders);

export default routerUpdate;