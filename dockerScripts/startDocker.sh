#!/bin/bash
docker rm -vf $(docker ps -a -q)
docker rmi -f $(docker images -a -q)
docker run --rm     -v /var/run/docker.sock:/var/run/docker.sock \
                    -v "$PWD:$PWD" -w="$PWD" \
                    docker/compose:1.24.0 up -d