import React, {useEffect} from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import {Layout} from "./components";
import { Home, Profile, Dashboard, Login, Register } from "./pages";
import "./App.css";
import {createTheme, ThemeProvider, useMediaQuery} from "@material-ui/core";
import {useState} from "react";
import {Brightness2, Brightness7} from "@material-ui/icons";

const App = () => {
  const prefersDarkTheme = useMediaQuery('(prefers-color-scheme: dark)');
  const [isDarkTheme, setIsDarkTheme] = useState(prefersDarkTheme);
  useEffect(() => {
    setIsDarkTheme(prefersDarkTheme);
  }, [prefersDarkTheme])
  const toggleDarkTheme = () => setIsDarkTheme(!isDarkTheme);
  const themeIcon = (isDarkTheme) ? <Brightness7 /> : <Brightness2 />;
  const theme = createTheme({
    palette: {
      type: isDarkTheme ? 'dark' : 'light',
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Layout toggleDarkTheme={toggleDarkTheme} themeIcon={themeIcon}>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/profile" component={Profile} />
            <Route path="/signin" component={Login} />
            <Route path="/signup" component={Register} />
          </Switch>
        </Layout>
      </Router>
    </ThemeProvider>
  );
};

export default App;
