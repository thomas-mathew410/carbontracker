import React from 'react';
import { mount } from 'enzyme';

import FlightTripForm from '../../FlightTripForm';

describe('FlightTripForm Component', () => {
    it('should render correctly and call onclose function when closed', () => {
        const closeFn = jest.fn();
        const component = mount(<FlightTripForm uid={999} handleClose={closeFn} />);
        expect(component).toMatchSnapshot();
        const buttons = component.find('button');
        expect(buttons.length).toEqual(6); // note-DS: this is 6 because of the additional 4 buttons built into the select inputs
        buttons.at(4).simulate('click');
        expect(closeFn).toHaveBeenCalled();
        component.unmount();
    });
})