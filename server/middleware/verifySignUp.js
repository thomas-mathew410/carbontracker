import {pool} from '../database/dbpool.js';

// could potentially add a check for email here as well 

const checkDuplicateUsername = async (req, res, next) => {
    await pool('person').where({username: req.body.username}).first().then((user) => {
        if (user) {
            res.status(400).send({message: "Sorry! Username is already in use."});
            return;
        }
        next();
    });
};

const verifySignup = {
    checkDuplicateUsername: checkDuplicateUsername,
}

export default verifySignup;