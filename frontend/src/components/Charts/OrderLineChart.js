import React, {useEffect, useState} from 'react';
import {ArgumentAxis, Chart, LineSeries, Title, ValueAxis} from '@devexpress/dx-react-chart-material-ui';
import AuthService from "../../services/auth.service";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => {
  return {
    chart: {
      maxHeight: 300,
      padding: theme.spacing(2),
    },
  };
});

const format = () => tick => tick;

const OrderLineChart = ({startDate, endDate}) => {
  const [user] = useState(AuthService.getCurrentUser());
  const [totalCarbonEmissions, setTotalCarbonEmissions] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    const startDateFormatted = startDate.toISOString().split('T')[0];
    const endDateFormatted = endDate.toISOString().split('T')[0];
    fetch(`/api/get/getCarbonOfOrdersWithDate/${user.uid}/${startDateFormatted}/${endDateFormatted}`)
      .then((res) => res.json())
      .then((data) => {
        const result = data.map((row) => {
          const dateComponents = row.date_added.split('T')[0].split('-');
          const dayMonth = `${dateComponents[1]}/${dateComponents[2]}`;
          return {
            'data': row.dateSum_carbon_g,
            'date': dayMonth,
          };
        });
        setTotalCarbonEmissions(result);
      });
  }, [user.uid, startDate, endDate]);

  return (
    <Chart className={classes.chart} data={totalCarbonEmissions}>
      <ArgumentAxis tickFormat={format} />
      <ValueAxis />
      <LineSeries
        valueField="data"
        argumentField="date"
      />
      <Title text="Trend"/>
    </Chart>
  );
};

export default OrderLineChart;
