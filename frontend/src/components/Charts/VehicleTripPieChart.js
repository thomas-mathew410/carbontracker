import React, {useEffect, useState} from 'react';
import {Chart, PieSeries, Title, Tooltip, Legend} from '@devexpress/dx-react-chart-material-ui';
import AuthService from "../../services/auth.service";
import {Animation, EventTracker} from '@devexpress/dx-react-chart';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => {
  return {
    chart: {
      maxHeight: 300,
      padding: theme.spacing(2),
    },
  };
});

const VehicleTripPieChart = ({startDate, endDate}) => {
  const [user] = useState(AuthService.getCurrentUser());
  const [totalCarbonEmissions, setTotalCarbonEmissions] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    const startDateFormatted = startDate.toISOString().split('T')[0];
    const endDateFormatted = endDate.toISOString().split('T')[0];
    fetch(`/api/get/getMonthlyCarbonOfVehiclesTrips/${user.uid}/${startDateFormatted}/${endDateFormatted}`)
      .then((res) => res.json())
      .then((data) => {
        const result = data.map((row) => {
          const date = new Date(row.month);
          date.setDate(1);
          const next= date.getMonth()+1;
          date.setMonth(next); // For PST??
          const month = date.toLocaleString('default', { month: 'long' });
          return {
            'month': month,
            'sum': row.monthly_carbon_g,
          };
        });
        setTotalCarbonEmissions(result);
      });
  }, [user.uid, startDate, endDate]);

  return (
    <Chart className={classes.chart} data={totalCarbonEmissions}>
      <PieSeries
        valueField="sum"
        argumentField="month"
      />
      <Title text="Monthly"/>
      <Legend />
      <EventTracker />
      <Tooltip />
      <Animation/>
    </Chart>
  );
};

export default VehicleTripPieChart;
