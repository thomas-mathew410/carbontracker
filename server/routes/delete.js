import express from 'express';
import {deleteFlight, 
        deleteOrder,
        deleteVehicleTrip,
        deleteVehicle,
        deletePerson,
        deleteGroup} from "../controllers/delete.js";

const deleteRoutes = express.Router();

deleteRoutes.delete("/deleteFlight/:fid", deleteFlight);
deleteRoutes.delete('/deleteVehicleTrip/:vtid', deleteVehicleTrip);
deleteRoutes.delete('/deleteOrder/:oid', deleteOrder);
deleteRoutes.delete('/deleteVehicle/:vid', deleteVehicle);

deleteRoutes.delete('/deletePerson/:uid', deletePerson);

deleteRoutes.delete('/deleteGroup/:uid/:gid', deleteGroup);

export default deleteRoutes;