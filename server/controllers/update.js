import {pool} from '../database/dbpool.js';
import axios from 'axios';
 
//         updateVehicleTrip, 
//         updateFlightTrip,
//         updateOrders 

const updatePersonQuery = async (pool, data) => {
    try {
        return await pool('person').update(data).where('uid', data["uid"]);
    } catch (err) {
        throw Error(err);
    }
};

export const updatePerson = async (req, res) => {
    try {
        var uid = req.body.uid;
        await updatePersonQuery(pool, req.body);
    } catch (err) {
        console.log(err)
        res.status(500)
            .send('Unable to update person; see logs for more details.')
            .end();
        return;
    }
    res.status(200).send(`{"uid": ${uid}}`).end();
}

const updateVehicleQuery = async (pool, data) => {
    try {
      return await pool('vehicle').update(data).where('vid', data["vid"]);
    } catch (err) {
      throw Error(err);
    }
  };

export const updateVehicle = async (req, res) => {
    try {
        var vid = req.body.vid;
        await updateVehicleQuery(pool, req.body);
    } catch (err) {
        console.log(err)
        res.status(500)
            .send('Unable to update vehicle; see logs for more details.')
            .end();
        return;
    }
    res.status(200).send(`{"vid": ${vid}}`).end();
}

const updateVehicleTripQuery = async (pool, data) => {
    try {
      return await pool('vehicletrips').update(data).where('vtid', data["vtid"]);
    } catch (err) {
      throw Error(err);
    }
  };

export const updateVehicleTrip = async (req, res) => {
    /*
    Be judicous with how you test this, only have 200 api calls per month...
    */
   var vtid = req.body.vtid;
    axios
    .post(process.env.CARBON_INTERFACE_URL, {
        type: "vehicle", //fixed value
        "distance_unit": req.body.distanceunit,
        "distance_value": req.body.distance,
        "vehicle_model_id": req.body.model_id
    }, {
        headers: {
            "Authorization": process.env.__CARBON_INTERFACE_API_KEY_PK
        }
    })
    .then(carbonRes => {
        const carbonData = carbonRes.data;
        const data = {
            vtid: req.body.vtid,
            vid: req.body.vid,
            uid: req.body.uid,
            distance: req.body.distance,
            distanceunit: req.body.distanceunit,
            model_id: req.body.model_id,
            carbon_g: carbonData.data.attributes.carbon_g,
            carbon_lb: carbonData.data.attributes.carbon_lb,
            carbon_kg: carbonData.data.attributes.carbon_kg,
            carbon_mt: carbonData.data.attributes.carbon_mt,
            date_added: req.body.date_added
        };
        try {
            updateVehicleTripQuery(pool, data)
            .then(_ => res.status(200).send(`{"vtid": ${vtid}}`).end());
        } catch (err) {
            console.log(err)
            return res.status(500)
                .send('Unable to cast vehicle trip; see logs for more details.')
                .end();
        }
    })
}

/*
    uid is fk, so if uid does not exist in person table, then this will fail
    req.body: passengers: int, strt: varchar, dst: varchar, carbonemissions
    IMPORTANT NOTE: req.body might change based on info required for carbon interface api
*/
const updateFlightTrips = async (pool, data) => {
    try {
      return await pool('flighttrips').update(data).where('fid', data["fid"]);
    } catch (err) {
      throw Error(err);
    }
  };

export const updateFlightTrip = async (req, res) => {
    var fid = req.body.fid;
    axios
        .post(process.env.CARBON_INTERFACE_URL, {
            "type": "flight",
            "passengers": req.body.passengers,
            "legs": [
                {"departure_airport": req.body.strt, "destination_airport": req.body.dst},
                {"departure_airport": req.body.dst, "destination_airport": req.body.strt}
            ]
        }, {
            headers: {
                "Authorization": process.env.__CARBON_INTERFACE_API_KEY_PK
            }
        })
        .then(carbonRes => {
            const carbonData = carbonRes.data;
            const data = {
                fid: req.body.fid,
                uid: req.body.uid,
                passengers: req.body.passengers,
                strt: req.body.strt,
                dst: req.body.dst,
                // date: carbonData.data.attributes.estimated_at,
                carbon_g: carbonData.data.attributes.carbon_g,
                carbon_lb: carbonData.data.attributes.carbon_lb,
                carbon_kg: carbonData.data.attributes.carbon_kg,
                carbon_mt: carbonData.data.attributes.carbon_mt,
                date_added: req.body.date_added
            };
        try {
            updateFlightTrips(pool, data)
             .then(_ => res.status(200).send(`{"fid": ${fid}}`).end());
        } catch (err) {
            console.log(err)
            return res.status(500)
                .send('Unable to cast flighttrips; see logs for more details.')
                .end();
        }})
}

/*
    uid is fk
    req.body: weight: numeric, weight_unit: varchar, distance: numeric, distance_unit: varchar, transmthd: varchar, carbonemissions
    req body might change
*/
const updateOrdersQuery = async (pool, data) => {
    try {
      return await pool('orders').update(data).where('oid', data["oid"]);
    } catch (err) {
      throw Error(err);
    }
  };

export const updateOrders = async (req, res) => {
    var oid = req.body.oid;
    axios.post(process.env.CARBON_INTERFACE_URL, {
            type: "shipping", //fixed value
            "weight_value": req.body.weight,
            "weight_unit": req.body.weight_unit,
            "distance_value": req.body.distance,
            "distance_unit": req.body.distance_unit,
            "transport_method": req.body.transmthd,
        }, {
            headers: {
                "Authorization": process.env.__CARBON_INTERFACE_API_KEY_PK
            }
        })
        .then(carbonRes => {
            const shippingCarbon = carbonRes.data;
            const data = {
                oid: req.body.oid,
                uid: req.body.uid,
                weight: req.body.weight,
                weight_unit: req.body.weight_unit,
                distance: req.body.distance,
                distance_unit: req.body.distance_unit,
                transmthd: req.body.transmthd,
                // date: shippingCarbon.data.attributes.estimated_at,
                carbon_g: shippingCarbon.data.attributes.carbon_g,
                carbon_lb: shippingCarbon.data.attributes.carbon_lb,
                carbon_kg: shippingCarbon.data.attributes.carbon_kg,
                carbon_mt: shippingCarbon.data.attributes.carbon_mt,
                date_added: req.body.date_added
            };
            try {
                updateOrdersQuery(pool, data)
                  .then(_ => res.status(200).send(`{"oid": ${oid}}`).end());
            } catch (err) {
                console.log(err)
                return res.status(500)
                    .send('Unable to cast orders; see logs for more details.')
                    .end();
            }
        })
}
