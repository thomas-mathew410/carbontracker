#!/bin/bash
#Login to docker hub before running the script so that the image will be pushed

docker image rm thomascmpt470/carbon-tracker-frontend:latest
docker build $PWD/frontend/ -t thomascmpt470/carbon-tracker-frontend:latest
docker commit thomascmpt470/carbon-tracker-frontend:latest
docker image push thomascmpt470/carbon-tracker-frontend:latest