import { allAccess, userBoard, adminBoard } from "../controllers/user.controller.js";
import express from 'express';
import authJwt from "../middleware/authJwt.js";

const userRouter = express.Router();


  userRouter.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  userRouter.get("/all", allAccess);

  userRouter.get(
    "/user",
    [authJwt.verifyToken],
    userBoard
  );

  userRouter.get(
    "/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    adminBoard
  );

  export default userRouter;