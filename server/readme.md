#This ReadMe is for developers in the team 

Please update readme as required

Instructions:
Server
- cd to server
- <strike> Create a .env file (we will use this file for environment variables) </strike> Pushed .env file to make it easier to develop
- npm install
- npm run dev (make sure nodemon is installed globally)

Frontend
- cd to 'frontend'
- npm install
- npm start

# CloudSQL for PostgreSQL DB

- after setting the connection variable 'pool', the way to get/post data is the same to use the local postgresql server.
- Code references: https://github.com/GoogleCloudPlatform/nodejs-docs-samples/tree/master/cloud-sql/postgres/knex
- Documentation: https://cloud.google.com/sql/docs/postgres
- Knex query builder reference : https://knexjs.org/#Builder-wheres

*Manage the DB in the CloudSQL directly

0. Go to the GCP and 'Carbon-Tracker' project (every member is invited to the project)
-- If you are running the server locally and trying to connect to Cloud SQL, make sure you whitelist your IP address (go to Cloud SQL instance -> connections -> add IP to authorized network)
1. In the Google Cloud Console, click the Cloud Shell icon (Cloud Shell icon) in the upper right corner.
2. run the command 'gcloud sql connect carbon-db --user=postgres' (The pwd for this user is 'carbon470') to connect to the instance 'carbon-db'
3. In the postgres server, run '\connect carbonemissions' to connect the DB in the instance (password is 'carbon470')
4. Then use the postsql!
   (* You might need to run 'CREATE EXTENSION citext' before managing tables)
   
- If you get some permission errors, let Laine know.
- The tables we need for the project are all created in the DB (empty tables).
- In GCP, search for cloudsql, then you could access to the managment pages of the DB instance and etc.

*Connecting to DB using proxy
run the following command in the shell
Start-Process -filepath "\path\to\proxy.exe\file\cloud_sql_proxy.exe" -ArgumentList "-instances=carbon-tracker-318418:northamerica-northeast1:carbon-db=tcp:5432 -credential_file=file\path\to\carbon-tracker-318418-4b6952af9497.json"

# Available routes
## get
- localhost:5000/api/get/getPeople (no req body)
- localhost:5000/api/get/findPerson/:uid 
- localhost:5000/api/get/getAllVehicles (no req body)
- localhost:5000/api/get/getAllVehiclesTrips (no req body)
- localhost:5000/api/get/getAllFlightTrips (no req body)
- localhost:5000/api/get/getAllOrders (no req body)

// (For line graphs) These return carbon amount of each date, summing the carbon amount in the same date within the date range
- localhost:5000/api/get/getCarbonOfVehiclesTripsWithDate/:uid/:startDate/:endDate
- localhost:5000/api/get/getCarbonOfOrdersWithDate/:uid/:startDate/:endDate
- localhost:5000/api/get/getCarbonOfFlightsWithDate/:uid/:startDate/:endDate

// (For pie graphs) These return total carbon amount within the date range
- localhost:5000/api/get/getTotalCarbonOfVehiclesTripsWithDate/:uid/:startDate/:endDate
- localhost:5000/api/get/getTotalCarbonOfOrdersWithDate/:uid/:startDate/:endDate
- localhost:5000/api/get/getTotalCarbonOfFlightsWithDate/:uid/:startDate/:endDate

// Weekly sum for each week, a week starts on Monday
- localhost:5000/api/get/getWeeklyCarbonOfVehiclesTrips/:uid
- localhost:5000/api/get/getWeeklyCarbonOfOrders/:uid
- localhost:5000/api/get/getWeeklyCarbonOfFlights/:uid

//Monthly carbon data
- localhost:5000/api/get/getMonthlyCarbonOfVehiclesTrips/:uid
- localhost:5000/api/get/getMonthlyCarbonOfOrders/:uid
- localhost:5000/api/get/getMonthlyCarbonOfFlights/:uid

// Group data
- localhost:5000/api/get/getAllGroups
- localhost:5000/api/get/getThreeLeastCarbonEmitters/:uid/:gid/:startDate/:endDate
- localhost:5000/api/get/isMemberOfAGroup/:uid
- localhost:5000/api/get/getUserGroupInfo/:uid

## post - see controllers/post.js for request body notes/params
- localhost:5000/api/post/addPerson
- localhost:5000/api/post/addVehicle
- localhost:5000/api/post/addVehicleTrip
- localhost:5000/api/post/addFlightTrip
- localhost:5000/api/post/addOrders
- localhost:5000/api/post/addGroup
- localhost:5000/api/post/joinGroup/:uid/:gid
- localhost:5000/api/post/leaveGroup/:uid/:gid
- localhost:5000/api/post/setUserGroup/:uid/:gid

## update - similar to add but uses the id generated during add for transactions
- localhost:5000/api/update/updatePerson
- localhost:5000/api/update/updateVehicle
- localhost:5000/api/update/updateVehicleTrip
- localhost:5000/api/update/updateFlightTrip
- localhost:5000/api/update/updateOrders 

## delete - the params are all primary key ID
- localhost:5000/api/delete/deleteFlight/:fid
- localhost:5000/api/delete/deleteVehicleTrip/:vtid
- localhost:5000/api/delete/deleteOrder/:oid
- localhost:5000/api/delete/deleteGroup/:uid/:gid



# Docker usage
To deploy containers on VM follow the steps below:
1. Make sure that the correct image is on docker hub. If unsure, simply go to your project directory and run:
   dockerScripts/dockerFrontend.sh     #to push fronend image based on local files
   dockerScripts/dockerServer.sh       #to push server image based on local files
2. After Step 1 simply turn on the VM (carbon-tracker ip: 34.134.198.86) to start the application. To stop simply turn it off. OR follow step 3.
3. Once the images are on docker hub. Copy docker-compose.yml, dockerScripts/startDocker.sh and dockerScripts/stopDocker.sh if they do not exist on the VM and run:
   cd /home/thomascmpt470              #I'm not sure if this would be possible
   bash startDocker.sh                 #to start the containers
   bash stopDocker.sh                  #to stop the containers

To run containers locally follow the steps below:
1. After making changes to the files run the following command:
   docker-compose -f docker-composeDev.yml up --build -d
2. Now frontend should be accessible via https://localhost:3000 and server should be accessible via https://localhost:5000
3. To go into any of the containers use:
   docker exec -it [name of container] /bin/bash[or /bin/sh]
4. To stop the containers run:
   docker-compose down

Currently the https certificates, for server, are stored at the root folder of server itself. This is temporary amd will be changed later.

# Admin Login
username: Admin

password: password

# Groups Feature
1. A user can belong to only one group, if a user wants to join a new group they will have to first leave their current group begore joining.
2. The person creating a group will be its manager. If the manager leaves the group, then it will cease to exist.
3. If a regular person leaves a group, it will continue to function as expected.
4. A group member can get the carbon emission of the three least emitters of their group.
