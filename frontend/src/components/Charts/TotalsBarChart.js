import React, {useEffect, useState} from 'react';
import {Chart, BarSeries, Title, ArgumentAxis, ValueAxis} from '@devexpress/dx-react-chart-material-ui';
import AuthService from "../../services/auth.service";
import {Animation} from '@devexpress/dx-react-chart';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => {
  return {
    chart: {
      maxHeight: 350,
      padding: theme.spacing(2),
    },
  };
});

const TotalsBarChart = () => {
  const [user] = useState(AuthService.getCurrentUser());
  const [totalCarbonEmissions, setTotalCarbonEmissions] = useState([]);
  const classes = useStyles();

  const fetchTotals = async () => {
    const res = await fetch(`/api/get/getTotalCarbon/${user.uid}`);
    return await res.json();
  }

  useEffect(() => {
    fetchTotals()
      .then((data) => setTotalCarbonEmissions(data));
  }, []);

  return (
    <Chart
      className={classes.chart}
      data={totalCarbonEmissions}
    >
      <ArgumentAxis />
      <ValueAxis />

      <BarSeries
        valueField="value"
        argumentField="label"
      />
      <Title text="Totals (in Grams)" />
      <Animation />
    </Chart>
  );
};

export default TotalsBarChart;
