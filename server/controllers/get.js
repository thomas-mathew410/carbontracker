import {pool} from '../database/dbpool.js';
import * as knex from "body-parser";

const allPeopleQuery = async pool => {
    return await pool
        .select('uid', 'username', 'country', 'gid')
        .from('person')
        .orderBy('country')
        .limit(100)
};

export const getPeople = async (req, res) => {
    try {
        const result = await allPeopleQuery(pool);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get people; see logs for more details.')
            .end();
    }
}

const findAPersonQuery = async (pool, id) => {
    return await pool
        .select('uid', 'username', 'country', 'gid')
        .from('person')
        .where('uid', id)
};

export const findPerson = async (req, res) => {
    try {
        const result = await findAPersonQuery(pool, req.params.uid);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get person; see logs for more details.')
            .end();
    }
}

const findUserGroupQuery = async (pool, id) => {
    return await pool
        .select('gid')
        .from('person')
        .where('uid', id)
};

export const findUserGroup = async (req, res) => {
    try {
        const result = await findUserGroupQuery(pool, req.params.uid);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get user group; see logs for more details.')
            .end();
    }
}



/* Flight Trips */

const allAirportQuery = async pool => {
    return await pool
        .select('iatacode', 'name', 'city', 'country')
        .from('airports')
        .orderBy('name')
        .limit(10000)
};

export const getAllAirports = async (req, res) => {
    try {
        const result = await allAirportQuery(pool);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get airports; see logs for more details.')
            .end();
    }
}

const allFlightTripsQuery = async (pool, uid) => {
    return await pool
        .select('fid', 'uid', 'passengers', 'strt', 'dst', 'carbon_g', 'carbon_lb', 'carbon_kg', 'carbon_mt', 'date_added')
        .from('flighttrips')
        .where('uid', uid)
        .orderBy('date_added')
        .limit(100)
};

export const getAllFlightTrips = async (req, res) => {
    try {
        const result = await allFlightTripsQuery(pool, req.params.uid);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get flight trips; see logs for more details.')
            .end();
    }
}

const allFlightTripsInDateRangeQuery = async (pool, uid, startDate, endDate) => {
    return await pool
      .select('fid', 'uid', 'passengers', 'strt', 'dst', 'carbon_g', 'carbon_lb', 'carbon_kg', 'carbon_mt', 'date_added')
      .from('flighttrips')
      .where('uid', uid)
      .whereBetween('date_added',[startDate, endDate])
      .orderBy('date_added')
      .limit(100)
};

export const getAllFlightTripsInDateRange = async (req, res) => {
    try {
        const result = await allFlightTripsInDateRangeQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
          .send('Unable to get flight trips; see logs for more details.')
          .end();
    }
}

const FlightsWithDateQuery = async (pool, uid, startDate, endDate) => {
    return await pool
        .select('date_added')
        .sum('carbon_g as dateSum_carbon_g')
        .sum('carbon_lb as dateSum_carbon_lb')
        .sum('carbon_kg as dateSum_carbon_kg')
        .sum('carbon_mt as dateSum_carbon_mt')
        .from('flighttrips')
        .where('uid', uid)
        .whereBetween('date_added',[startDate, endDate])
        .groupBy('date_added')
        .orderBy('date_added');

};

export const getCarbonOfFlightsWithDate = async (req, res) => {
    try {
        const result = await FlightsWithDateQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get flight Trips within the date range; see logs for more details.')
            .end();
    }
}

const TotalCarbonFlightsWithDateQuery = async (pool, uid, startDate, endDate) => {
    return await pool
        .sum('carbon_g as flight_dateSum_carbon_g')
        // .sum('carbon_lb as dateSum_carbon_lb')
        // .sum('carbon_kg as dateSum_carbon_kg')
        // .sum('carbon_mt as dateSum_carbon_mt')
        .from('flighttrips')
        .where('uid', uid)
        .whereBetween('date_added',[startDate, endDate]);
};

export const getTotalCarbonOfFlightsWithDate = async (req, res) => {
    try {
        const result = await TotalCarbonFlightsWithDateQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get flight Trips within the date range; see logs for more details.')
            .end();
    }
}

const WeeklyCarbonFlightsQuery = async (pool, uid) => {
    return await pool
        .raw(`SELECT DATE_TRUNC('week', date_added) AS week, COUNT(date_added) AS weekly_count,
                     SUM(carbon_g) AS weekly_carbon_g, SUM(carbon_lb) AS weekly_carbon_lb, 
                     SUM(carbon_kg) AS weekly_carbon_kg, SUM(carbon_mt) AS weekly_carbon_mt
            FROM flighttrips WHERE uid = (?) 
            GROUP BY DATE_TRUNC('week', date_added) ORDER BY week`
            , uid)
};

export const getWeeklyCarbonOfFlights = async (req, res) => {
    try {
        const result = await WeeklyCarbonFlightsQuery(pool, req.params.uid);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get the result; see logs for more details.')
            .end();
    }
}

const MonthlyCarbonFlightsQuery = async (pool, uid, startDate, endDate) => {
    return await pool
        .raw(`select DATE_TRUNC('month', date_added) AS month, COUNT(date_added) AS monthly_count,
                     SUM(carbon_g) AS monthly_carbon_g, SUM(carbon_lb) AS monthly_carbon_lb, 
                     SUM(carbon_kg) AS monthly_carbon_kg, SUM(carbon_mt) AS monthly_carbon_mt 
            FROM flighttrips
            WHERE uid = (?) AND date_added >= (?) AND date_added <= (?)
            GROUP BY DATE_TRUNC('month', date_added) ORDER BY month`
            , [uid, startDate, endDate])
};

export const getMonthlyCarbonOfFlights = async (req, res) => {
    try {
        const result = await MonthlyCarbonFlightsQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result.rows).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get the result; see logs for more details.')
            .end();
    }
}

/* Orders */

const allOrdersQuery = async (pool, uid) => {
    return await pool
        .select('oid', 'uid', 'weight', 'weight_unit', 'distance', 'distance_unit', 'transmthd', 'carbon_g', 'carbon_lb', 'carbon_kg', 'carbon_mt', 'date_added')
        .from('orders')
        .where('uid', uid)
        .orderBy('date_added')
        .limit(100)
};

export const getAllOrders = async (req, res) => {
    try {
        const result = await allOrdersQuery(pool, req.params.uid);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get orders trips; see logs for more details.')
            .end();
    }
}

const allOrdersInDateRangeQuery = async (pool, uid, startDate, endDate) => {
    return await pool
      .select('oid', 'uid', 'weight', 'weight_unit', 'distance', 'distance_unit', 'transmthd', 'carbon_g', 'carbon_lb', 'carbon_kg', 'carbon_mt', 'date_added')
      .from('orders')
      .where('uid', uid)
      .whereBetween('date_added',[startDate, endDate])
      .orderBy('date_added')
      .limit(100)
};

export const getAllOrdersInDateRange = async (req, res) => {
    try {
        const result = await allOrdersInDateRangeQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
          .send('Unable to get orders trips; see logs for more details.')
          .end();
    }
}

const OrdersWithDateQuery = async (pool, uid, startDate, endDate) => {
    return await pool
        .select('date_added')
        .sum('carbon_g as dateSum_carbon_g')
        .sum('carbon_lb as dateSum_carbon_lb')
        .sum('carbon_kg as dateSum_carbon_kg')
        .sum('carbon_mt as dateSum_carbon_mt')
        .from('orders')
        .where('uid', uid)
        .whereBetween('date_added',[startDate, endDate])
        .groupBy('date_added')
        .orderBy('date_added');
};

export const getCarbonOfOrdersWithDate = async (req, res) => {
    try {
        const result = await OrdersWithDateQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get order within the date range; see logs for more details.')
            .end();
    }
}

const TotalOrdersWithDateQuery = async (pool, uid, startDate, endDate) => {
    return await pool
        .sum('carbon_g as orders_dateSum_carbon_g')
        // .sum('carbon_lb as dateSum_carbon_lb')
        // .sum('carbon_kg as dateSum_carbon_kg')
        // .sum('carbon_mt as dateSum_carbon_mt')
        .from('orders')
        .where('uid', uid)
        .whereBetween('date_added',[startDate, endDate])
};

export const getTotalCarbonOfOrdersWithDate = async (req, res) => {
    try {
        const result = await TotalOrdersWithDateQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get order within the date range; see logs for more details.')
            .end();
    }
}


const MonthlyCarbonOrdersQuery = async (pool, uid, startDate, endDate) => {
    return await pool
        .raw(`select DATE_TRUNC('month', date_added) AS month, COUNT(date_added) AS monthly_count,
                     SUM(carbon_g) AS monthly_carbon_g, SUM(carbon_lb) AS monthly_carbon_lb, 
                     SUM(carbon_kg) AS monthly_carbon_kg, SUM(carbon_mt) AS monthly_carbon_mt 
            FROM orders
            WHERE uid = (?) AND date_added >= (?) AND date_added <= (?) 
            GROUP BY DATE_TRUNC('month', date_added) ORDER BY month`
            , [uid, startDate, endDate])
};

export const getMonthlyCarbonOfOrders = async (req, res) => {
    try {
        const result = await MonthlyCarbonOrdersQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result.rows).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get the result; see logs for more details.')
            .end();
    }
}


const WeeklyCarbonOrdersQuery = async (pool, uid) => {
    return await pool
        .raw(`SELECT DATE_TRUNC('week', date_added) AS week, COUNT(date_added) AS weekly_count,
                     SUM(carbon_g) AS weekly_carbon_g, SUM(carbon_lb) AS weekly_carbon_lb, 
                     SUM(carbon_kg) AS weekly_carbon_kg, SUM(carbon_mt) AS weekly_carbon_mt
            FROM orders WHERE uid = (?) 
            GROUP BY DATE_TRUNC('week', date_added) ORDER BY week`
            , uid)
};

export const getWeeklyCarbonOfOrders = async (req, res) => {
    try {
        const result = await WeeklyCarbonOrdersQuery(pool, req.params.uid);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get the result; see logs for more details.')
            .end();
    }
}

/* Vehicle Trips */
const allVehicleTripsQuery = async (pool, uid) => {
    return await pool
        .select('vtid', 'vid', 'uid', 'distance', 'distanceunit', 'model_id', 'carbon_g', 'carbon_lb', 'carbon_kg', 'carbon_mt', 'date_added')
        .from('vehicletrips')
        .where('uid', uid)
        .orderBy('date_added');
};

export const getAllVehiclesTrips = async (req, res) => {
    try {
        const result = await allVehicleTripsQuery(pool, req.params.uid);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get vehicles trips; see logs for more details.')
            .end();
    }
}

const allVehicleTripsInDateRangeQuery = async (pool, uid, startDate, endDate) => {
    return await pool
      .select('vtid', 'vid', 'uid', 'distance', 'distanceunit', 'model_id', 'carbon_g', 'carbon_lb', 'carbon_kg', 'carbon_mt', 'date_added')
      .from('vehicletrips')
      .where('uid', uid)
      .whereBetween('date_added',[startDate, endDate])
      .orderBy('date_added');
};

export const getAllVehiclesTripsInDateRange = async (req, res) => {
    try {
        const result = await allVehicleTripsInDateRangeQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
          .send('Unable to get vehicles trips; see logs for more details.')
          .end();
    }
}


const VehicleTripsQueryWithDate = async (pool, uid, startDate, endDate) => {
    return await pool
        .select('date_added')
        .sum('carbon_g as dateSum_carbon_g')
        .sum('carbon_lb as dateSum_carbon_lb')
        .sum('carbon_kg as dateSum_carbon_kg')
        .sum('carbon_mt as dateSum_carbon_mt')
        .from('vehicletrips')
        .where('uid', uid)
        .whereBetween('date_added',[startDate, endDate])
        .groupBy('date_added')
        .orderBy('date_added');
};

export const getCarbonOfVehiclesTripsWithDate = async (req, res) => {
    try {
        const result = await VehicleTripsQueryWithDate(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get vehicles trips with the date range; see logs for more details.')
            .end();
    }
}


const TotalCarbonVehicleTripsQueryWithDate = async (pool, uid, startDate, endDate) => {
    return await pool
        .sum('carbon_g as vt_dateSum_carbon_g')
        // .sum('carbon_lb as dateSum_carbon_lb')
        // .sum('carbon_kg as dateSum_carbon_kg')
        // .sum('carbon_mt as dateSum_carbon_mt')
        .from('vehicletrips')
        .where('uid', uid)
        .whereBetween('date_added',[startDate, endDate])
};

export const getTotalCarbonOfVehiclesTripsWithDate = async (req, res) => {
    try {
        const result = await TotalCarbonVehicleTripsQueryWithDate(pool, req.params.uid, req.params.startDate, req.params.endDate);
        const result2 = await TotalOrdersWithDateQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        const result3 = await TotalCarbonFlightsWithDateQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        var data = []
        const keyValues = {
            flight_dateSum_carbon_g: "Flight",
            vt_dateSum_carbon_g: "Vehicle",
            orders_dateSum_carbon_g: "Orders"
        }
        for (const key in result3[0]) {
            var obj = {
                value: result3[0][key] || 0,
                label: keyValues[key]
            }
            data.push(obj);
        }
        for (const key in result[0]) {
            var obj = {
                value: result[0][key] || 0,
                label: keyValues[key]
            }
            data.push(obj);
        }
        for (const key in result2[0]) {
            var obj = {
                value: result2[0][key] || 0,
                label: keyValues[key]
            }
            data.push(obj);
        }
        res.status(200).send(data).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get vehicles trips with the date range; see logs for more details.')
            .end();
    }
}


const MonthlyCarbonVehicleTripsQuery = async (pool, uid, startDate, endDate) => {
    return await pool
        .raw(`select DATE_TRUNC('month', date_added) AS month, COUNT(date_added) AS monthly_count,
                     SUM(carbon_g) AS monthly_carbon_g, SUM(carbon_lb) AS monthly_carbon_lb, 
                     SUM(carbon_kg) AS monthly_carbon_kg, SUM(carbon_mt) AS monthly_carbon_mt 
            FROM vehicletrips
            WHERE uid = (?) AND date_added >= (?) AND date_added <= (?) 
            GROUP BY DATE_TRUNC('month', date_added) ORDER BY month`
            , [uid, startDate, endDate])
};

export const getMonthlyCarbonOfVehiclesTrips = async (req, res) => {
    try {
        const result = await MonthlyCarbonVehicleTripsQuery(pool, req.params.uid, req.params.startDate, req.params.endDate);
        res.status(200).send(result.rows).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get the result; see logs for more details.')
            .end();
    }
}

const WeeklyCarbonVehicleTripsQuery = async (pool, uid) => {
    return await pool
        .raw(`SELECT DATE_TRUNC('week', date_added) AS week, COUNT(date_added) AS weekly_count,
                     SUM(carbon_g) AS weekly_carbon_g, SUM(carbon_lb) AS weekly_carbon_lb, 
                     SUM(carbon_kg) AS weekly_carbon_kg, SUM(carbon_mt) AS weekly_carbon_mt
            FROM vehicletrips WHERE uid = (?) 
            GROUP BY DATE_TRUNC('week', date_added) ORDER BY week`
            , uid)
};

export const getWeeklyCarbonOfVehiclesTrips = async (req, res) => {
    try {
        const result = await WeeklyCarbonVehicleTripsQuery(pool, req.params.uid);
        res.status(200).send(result.rows).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get the result; see logs for more details.')
            .end();
    }
}

/* Vehicle Models */

const uniqueVehicleMakes = async (pool, id) => {
    return await pool
        .select('make')
        .distinctOn('make')
        .from('vehiclemodels');
};

export const getUniqueVehicleMakes = async (req, res) => {
    try {
        const result = await uniqueVehicleMakes(pool, req.body);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get unique vehicle makes; see logs for more details.')
            .end();
    }
}

const vehicleYears = async (pool, make) => {
    return await pool
        .select('year')
        .distinctOn('year')
        .from('vehiclemodels')
        .where('make', make)
        .orderBy('year');
};

export const getAvailVehicleYears = async (req, res) => {
    try {
        const result = await vehicleYears(pool, req.params.make);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get unique vehicle makes; see logs for more details.')
            .end();
    }
}

const vehicleModels = async (pool, make, year) => {
    return await pool
        .select('model')
        .distinctOn('model')
        .from('vehiclemodels')
        .where('make', make)
        .andWhere('year', year)
        .orderBy('model');
};

export const getAvailVehicleModels = async (req, res) => {
    try {
        const result = await vehicleModels(pool, req.params.make, req.params.year);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get unique vehicle makes; see logs for more details.')
            .end();
    }
}

const vehicleModelId = async (pool, make, year, model) => {
    return await pool
        .select('id')
        .from('vehiclemodels')
        .where('make', make)
        .andWhere('year', year)
        .andWhere('model', model)
};

export const getVehicleModelId = async (req, res) => {
    try {
        const result = await vehicleModelId(pool, req.params.make, req.params.year, req.params.model);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get vehicles; see logs for more details.')
            .end();
    }
}

const allVehicleQuery = async (pool, uid) => {
    return await pool
        .select('vid', 'uid', 'make', 'model', 'year')
        .from('vehicle')
        .where('uid', uid)
        .orderBy('vid')
        .limit(100)
};

export const getAllVehicles = async (req, res) => {
    try {
        const result = await allVehicleQuery(pool, req.params.uid);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get vehicles; see logs for more details.')
            .end();
    }
}

const person_carbon_table_query = async (pool, uid) => {
    return await pool
        .select('totcarbon_flight_g', 'totcarbon_vt_g', 'totcarbon_ord_g')
        .from('person_carbon_emissions')
        .where('uid', uid)
};

export const getTotalCarbon = async (req, res) => {
    try {
        const result = await person_carbon_table_query(pool, req.params.uid);
        var data = []
        const keyValues = {
            totcarbon_flight_g: "Flight",
            totcarbon_vt_g: "Vehicle",
            totcarbon_ord_g: "Orders"
        }

        for (const key in result[0]) {
            var obj = {
                value: result[0][key] || 0,
                label: keyValues[key]
            }
            data.push(obj);
        }
        res.status(200).send(data).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get orders trips; see logs for more details.')
            .end();
    }
};

const allGroupsQuery = async (pool) => {
    return await pool
        .select('gid', 'gname', 'uid', 'mname', 'created_at', 'membernum')
        .from('groups')
        .orderBy('gid')
        .limit(100)
};

export const getAllGroups = async (req, res) => {
    try {
        const result = await allGroupsQuery(pool);
        res.status(200).send(result).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get groups; see logs for more details.')
            .end();
    }
}

const getGidOfPersonQuery = async (pool, uid) => {
    return await pool
        .raw(`SELECT gid
              FROM person
              WHERE uid=?;`, [uid]);
};

const leastCarbonWithDateQuery = async (pool, uid, gid, startDate, endDate) => {
    const gidOfPerson = await getGidOfPersonQuery(pool, uid);
    if(gidOfPerson.rows[0].gid == gid)
    {
        return await pool
            .raw(`SELECT temp.uid, temp.username, temp.gid, SUM( CASE  WHEN temp.carbon_g IS NULL THEN 0
                                                        ELSE temp.carbon_g
                                                        END) AS carbon_g,
                                             SUM( CASE  WHEN temp.carbon_lb IS NULL THEN 0
                                                        ELSE temp.carbon_lb
                                                        END) AS carbon_lb,
                                             SUM( CASE  WHEN temp.carbon_kg IS NULL THEN 0
                                                        ELSE temp.carbon_kg
                                                        END) AS carbon_kg,
                                             SUM( CASE  WHEN temp.carbon_mt IS NULL THEN 0
                                                        ELSE temp.carbon_mt
                                                        END) AS carbon_mt
                FROM
                    ((
                        SELECT person.uid AS uid, person.username AS username, person.gid AS gid, vehicletrips.carbon_g AS carbon_g, vehicletrips.carbon_lb AS carbon_lb,
                        vehicletrips.carbon_kg AS carbon_kg, vehicletrips.carbon_mt AS carbon_mt, vehicletrips.date_added as date_added
                        FROM person LEFT JOIN vehicletrips ON (person.uid = vehicletrips.uid)
                    )
                    UNION ALL
                    (
                        SELECT person.uid AS uid, person.username AS username, person.gid AS gid, flighttrips.carbon_g AS carbon_g, flighttrips.carbon_lb AS carbon_lb,
                        flighttrips.carbon_kg AS carbon_kg, flighttrips.carbon_mt AS carbon_mt, flighttrips.date_added as date_added
                        FROM person LEFT JOIN flighttrips ON (person.uid = flighttrips.uid)
                    )
                    UNION ALL
                    (
                        SELECT person.uid AS uid, person.username AS username, person.gid AS gid, orders.carbon_g AS carbon_g, orders.carbon_lb AS carbon_lb,
                        orders.carbon_kg AS carbon_kg, orders.carbon_mt AS carbon_mt, orders.date_added as date_added
                        FROM person LEFT JOIN orders ON (person.uid = orders.uid)
                    )) AS temp
                WHERE (temp.date_added > ? AND temp.date_added < ? OR temp.date_added IS NULL) AND temp.gid = ?
                GROUP BY temp.uid, temp.gid, temp.username
                ORDER by carbon_g
                LIMIT 3;`, [startDate, endDate, gid]);
    }
    throw 'Only group members can access the info';
};

export const getThreeLeastCarbonEmitters = async (req, res) => {
    try {
        const leastEmittingMembers = await leastCarbonWithDateQuery(pool, req.params.uid, req.params.gid, req.params.startDate, req.params.endDate);
        res.status(200).send(leastEmittingMembers).end();
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get least emitting members; see logs for more details.')
            .end();
    }
}

const isMemberOfAGroupQuery = async (pool, uid) => {
    return await pool
        .raw(`SELECT gid IS NOT NULL AS ismember
              FROM person
              WHERE uid=?;`, [uid]);
};

export const isMemberOfAGroup = async (req, res) => {
    try {
        var isMember = await isMemberOfAGroupQuery(pool, req.params.uid);
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get the group of the user; see logs for more details.')
            .end();
    }
    res.status(200).send(isMember.rows[0]).end();
}


const groupNameQuery = async (pool, gid) => {
    return await pool
        .select('gid', 'gname', 'mname', 'uid', 'created_at', 'membernum')
        .from('groups')
        .where('gid', gid)
};

export const getUserGroupInfo = async (req, res) => {
    try {
        const gidOfPerson = await getGidOfPersonQuery(pool, req.params.uid);
        const result = await groupNameQuery(pool, gidOfPerson.rows[0].gid);
        if (result == null){
            res.status(200).send('User does not belong to a group').end();
        }else{
            res.status(200).send(result).end();
        }
    }catch(err){
        console.error(err);
        res.status(500)
            .send('Unable to get the group info; see logs for more details.')
            .end();
    }
}