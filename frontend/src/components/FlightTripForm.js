import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Loading } from './index'
import { getToday } from '../utils/helpers';


const FlightTripForm = ({handleClose, uid}) => {
  const [numPassengers, setNumPassengers] = useState('');
  const [passengersError, setPassengersError] = useState(false);

  const [selectedStartLocation, setSelectedStartLocation] = useState('');
  const [startLocationError, setStartLocationError] = useState(false);
  const [dateAdded, setDateAdded] = useState(getToday());
  const [dateError, setDateError] = useState(false);

  const [selectedDestination, setSelectedDestination] = useState('');
  const [destinationError, setDestinationError] = useState(false);
  const [allAirports, setAllAirports] = useState([]);

  const [loading, setLoading] = useState(false);


  useEffect(() => {
    fetch('/api/get/getAllAirports')
        .then((res) => res.json())
        .then((data) => {
          setAllAirports(data);
        });
  }, []);

  const onAddFlightTrip = () => {
    const startEmpty = selectedStartLocation === '';
    const destinationEmpty = selectedDestination === '';
    const num = parseInt(numPassengers);
    const passengersNaN = isNaN(num);
    const invalidDate = dateAdded === '';
    if (startEmpty || destinationEmpty || passengersNaN || invalidDate) {
      setStartLocationError(startEmpty);
      setDestinationError(destinationEmpty);
      setPassengersError(passengersNaN);
      setDateError(invalidDate);
      return;
    }
    setLoading(true);

    const flightTrip = {
      'uid': uid,
      'passengers': numPassengers,
      'strt': selectedStartLocation,
      'dst': selectedDestination,
      'date_added': dateAdded
    }

    fetch('/api/post/addFlightTrip', {
      method: 'POST',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify(flightTrip),
    }).then((res) => {
      if (res.ok) {
        console.log('Successfully added flight trip');
      } else {
        console.log('Error adding flight trip');
      }
      setLoading(false);
      handleClose();
    });
  };

  return (
      <Grid container direction="column">
        <Grid item>
          <TextField
            required
            fullWidth
            variant="outlined"
            margin="normal"
            label="Passengers"
            InputProps={{name: 'passengers'}}
            value={numPassengers}
            onChange={e => {
              setNumPassengers(e.target.value);
            }}
            error={passengersError}
          />
        </Grid>
          <Autocomplete fullWidth
                id="Start"
                options={allAirports}
                getOptionLabel={(airport) => `${airport.iatacode}, ${airport.name}, ${airport.city}, ${airport.country}`}
                renderInput={(params) => <TextField {...params} required error={startLocationError} label="Start" variant="outlined" margin="normal"/>}
                onChange={(e, value) => {
                    if (value) {setSelectedStartLocation(value.iatacode);}
                }}
          />
          <Autocomplete fullWidth
              id="Destination"
              options={allAirports}
              getOptionLabel={(airport) => `${airport.iatacode}, ${airport.name}, ${airport.city}, ${airport.country}`}
              renderInput={(params) => <TextField {...params} required error={destinationError} label="Destination" variant="outlined" margin="normal" />}
              onChange={(e, value) => {
                  if (value) { setSelectedDestination(value.iatacode);}}}
          />
          <Grid item>
            <TextField
              id="date"
              label="Trip date"
              type="date"
              defaultValue={getToday()}
              onChange={e => {
                setDateAdded(e.target.value);
              }}
              InputLabelProps={{
                shrink: true,
              }}
              required
              error={dateError}

            />
          </Grid>
        <Grid container spacing={2} justifyContent="flex-end">
          <Grid item>
            <Button variant="text" onClick={handleClose}>Cancel</Button>
          </Grid>
          <Grid item>
            <Button variant="contained" onClick={onAddFlightTrip}>Add</Button>
          </Grid>
        </Grid>
        <Loading isLoading={loading} />
      </Grid>
  );
};

export default FlightTripForm;
