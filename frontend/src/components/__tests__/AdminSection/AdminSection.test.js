import React from 'react';
import { shallow } from 'enzyme';

import AdminSection from '../../AdminSection';

describe('AdminSection Component', () => {
    it('should render correctly', () => {
        const component = shallow(<AdminSection />);
        expect(component).toMatchSnapshot();
    });
})