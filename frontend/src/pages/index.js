import Home from './Home'
import Profile from './Profile'
import Dashboard from './Dashboard'
import Login from './Login'
import Register from './Register'
import VisualizeData from './VisualizeData'

export {Home, Profile, Dashboard, Login, Register, VisualizeData};
