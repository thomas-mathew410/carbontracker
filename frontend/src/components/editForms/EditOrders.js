import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";

import { Loading } from '../index'
import { getToday } from '../../utils/helpers';

const EditOrders = ({uid, oid, onClose}) => {
    const [distance, setDistance] = useState('');
    const [distanceError, setDistanceError] = useState(false);
    const [distanceUnit, setDistanceUnit] = useState('km');

    const [weight, setWeight] = useState('');
    const [weightError, setWeightError] = useState(false);
    const [weightUnit, setWeightUnit] = useState('kg');
    const [dateAdded, setDateAdded] = useState(getToday());
    const [dateError, setDateError] = useState(false);

    const [shippingMethod, setShippingMethod] = useState('plane');

    const [loading, setLoading] = useState(false);

    const onEditOrder = () => {
        const distanceNum = parseInt(distance);
        const distanceNaN = isNaN(distanceNum);
        const weightNum = parseInt(weight);
        const weightNaN = isNaN(weightNum);
        const invalidDate = dateAdded === '';
    
        if (distanceNaN || weightNaN || invalidDate) {
          setDistanceError(distanceNaN);
          setWeightError(weightNaN);
          setDateError(invalidDate);
          console.log('parsing error');
          return;
        }
        setLoading(true);
        const newOrder = {
          'oid': oid,
          'uid': uid,
          'weight': weightNum,
          'weight_unit': weightUnit,
          'distance': distanceNum,
          'distance_unit': distanceUnit,
          'transmthd': shippingMethod,
          'date_added': dateAdded
        }
    
            fetch('/api/update/updateOrders', {
                method: 'POST',
                headers: {'Content-type': 'application/json'},
                body: JSON.stringify(newOrder),
                }).then((res) => {
                    if (res.ok) {
                        console.log('Successfully editted order');
                        onClose();
                    } else {
                        console.log('Error editting order');
                        onClose();
                    }
                setLoading(false);
                });
        };

      return (
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <TextField
              required
              fullWidth
              variant="outlined"
              label="Distance"
              InputProps={{name: 'distance'}}
              value={distance}
              onChange={e => {
                setDistance(e.target.value);
              }}
              error={distanceError}
            />
          </Grid>
          <Grid item>
            <FormControl component="fieldset">
              <FormLabel component="legend">Distance Unit</FormLabel>
              <RadioGroup
                row
                name="distanceUnit"
                value={distanceUnit}
                onChange={(e) => setDistanceUnit(e.target.value)}
              >
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Kilometers"
                  value="km"
                  labelPlacement="start"
                />
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Miles"
                  value="mi"
                  labelPlacement="start"
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item>
            <TextField
              required
              fullWidth
              variant="outlined"
              label="Weight"
              InputProps={{name: 'weight'}}
              value={weight}
              onChange={e => {
                setWeight(e.target.value);
              }}
              error={weightError}
            />
          </Grid>
          <Grid item>
            <FormControl component="fieldset">
              <FormLabel component="legend">Weight Unit</FormLabel>
              <RadioGroup
                row
                name="weightunit"
                value={weightUnit}
                onChange={(e) => setWeightUnit(e.target.value)}
              >
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Grams"
                  value="g"
                  labelPlacement="start"
                />
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Pounds"
                  value="lb"
                  labelPlacement="start"
                />
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Kilograms"
                  value="kg"
                  labelPlacement="start"
                />
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Tonnes"
                  value="mt"
                  labelPlacement="start"
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item>
            <FormControl component="fieldset">
              <FormLabel component="legend">Shipping Method</FormLabel>
              <RadioGroup
                row
                name="shipmethod"
                value={shippingMethod}
                onChange={(e) => setShippingMethod(e.target.value)}
              >
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Ship"
                  value="ship"
                  labelPlacement="start"
                />
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Train"
                  value="train"
                  labelPlacement="start"
                />
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Truck"
                  value="truck"
                  labelPlacement="start"
                />
                <FormControlLabel
                  control={<Radio color="secondary" />}
                  label="Plane"
                  value="plane"
                  labelPlacement="start"
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item>
          <TextField
                id="date"
                label="Order date"
                type="date"
                defaultValue={getToday()}
                onChange={e => {
                  setDateAdded(e.target.value);
                }}
                InputLabelProps={{
                  shrink: true,
                }}
                required
                error={dateError}
              />
          </Grid>
          <Grid container spacing={2} justifyContent="flex-end">
            <Grid item>
              <Button variant="text" onClick={onClose}>Cancel</Button>
            </Grid>
            <Grid item>
              <Button variant="contained" onClick={onEditOrder}>Edit</Button>
            </Grid>
          </Grid>
          <Loading isLoading={loading}/>
        </Grid>
      );
}

export default EditOrders; 