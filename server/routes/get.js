// Add all the get routes here
// logic (call back function) in the get request is in controller/get.js
import express from 'express';

import {
    getPeople,
    findPerson,
    getAllVehicles,
    getAllVehiclesTrips,
    getAllFlightTrips,
    getAllOrders,
    getUniqueVehicleMakes,
    getAvailVehicleYears,
    getAvailVehicleModels,
    getTotalCarbon,
    getVehicleModelId,
    getAllAirports,
    getCarbonOfVehiclesTripsWithDate,
    getCarbonOfOrdersWithDate,
    getCarbonOfFlightsWithDate,
    getTotalCarbonOfVehiclesTripsWithDate,
    getTotalCarbonOfFlightsWithDate,
    getTotalCarbonOfOrdersWithDate,
    getMonthlyCarbonOfVehiclesTrips,
    getWeeklyCarbonOfVehiclesTrips,
    getMonthlyCarbonOfOrders, getWeeklyCarbonOfOrders, getWeeklyCarbonOfFlights, getMonthlyCarbonOfFlights,
    getAllFlightTripsInDateRange,
    getAllOrdersInDateRange,
    getAllVehiclesTripsInDateRange,
    getAllGroups,
    getThreeLeastCarbonEmitters,
    isMemberOfAGroup, getUserGroupInfo, findUserGroup
} from '../controllers/get.js';

const routerGet = express.Router();

//TODO add authJwt.js to all calls that should be verified (check ./middleware)

routerGet.use(function(req, res, next) {
    res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
    );
    next();
});

routerGet.get("/getPeople", getPeople);

routerGet.get("/findPerson/:uid", findPerson);
routerGet.get("/findGroupId/:uid", findUserGroup);

routerGet.get("/getUniqueVehicleMakes", getUniqueVehicleMakes);
routerGet.get("/getAvailVehicleYears/:make", getAvailVehicleYears);
routerGet.get("/getAvailVehicleModels/:make/:year", getAvailVehicleModels);
routerGet.get("/getVehicleModelId/:make/:year/:model", getVehicleModelId);
routerGet.get("/getAllVehicles/:uid", getAllVehicles);
routerGet.get("/getAllAirports", getAllAirports);

routerGet.get("/getAllVehiclesTrips/:uid", getAllVehiclesTrips);
routerGet.get("/getAllFlightTrips/:uid", getAllFlightTrips);
routerGet.get("/getAllOrders/:uid", getAllOrders);
routerGet.get("/getAllGroups", getAllGroups);
routerGet.get("/getUserGroupInfo/:uid", getUserGroupInfo );

// (For line graphs) These return carbon amount of each date, summing the carbon amount in the same date within the date range
routerGet.get("/getCarbonOfVehiclesTripsWithDate/:uid/:startDate/:endDate", getCarbonOfVehiclesTripsWithDate);
routerGet.get("/getCarbonOfOrdersWithDate/:uid/:startDate/:endDate", getCarbonOfOrdersWithDate);
routerGet.get("/getCarbonOfFlightsWithDate/:uid/:startDate/:endDate", getCarbonOfFlightsWithDate);

// (For pie graphs) These return total carbon amount within the date range
routerGet.get("/getTotalCarbonOfVehiclesTripsWithDate/:uid/:startDate/:endDate", getTotalCarbonOfVehiclesTripsWithDate);
routerGet.get("/getTotalCarbonOfOrdersWithDate/:uid/:startDate/:endDate", getTotalCarbonOfOrdersWithDate);
routerGet.get("/getTotalCarbonOfFlightsWithDate/:uid/:startDate/:endDate", getTotalCarbonOfFlightsWithDate);

//Weekly carbon data, a week starts on Monday
routerGet.get("/getWeeklyCarbonOfVehiclesTrips/:uid", getWeeklyCarbonOfVehiclesTrips);
routerGet.get("/getWeeklyCarbonOfOrders/:uid", getWeeklyCarbonOfOrders);
routerGet.get("/getWeeklyCarbonOfFlights/:uid", getWeeklyCarbonOfFlights);

//Monthly carbon data
routerGet.get("/getMonthlyCarbonOfVehiclesTrips/:uid/:startDate/:endDate", getMonthlyCarbonOfVehiclesTrips);
routerGet.get("/getMonthlyCarbonOfOrders/:uid/:startDate/:endDate", getMonthlyCarbonOfOrders);
routerGet.get("/getMonthlyCarbonOfFlights/:uid/:startDate/:endDate", getMonthlyCarbonOfFlights);

routerGet.get("/getTotalCarbon/:uid", getTotalCarbon);
// Group carbon data
routerGet.get("/getThreeLeastCarbonEmitters/:uid/:gid/:startDate/:endDate", getThreeLeastCarbonEmitters);
routerGet.get("/isMemberOfAGroup/:uid", isMemberOfAGroup);

routerGet.get("/getAllFlightTripsInDateRange/:uid/:startDate/:endDate", getAllFlightTripsInDateRange);
routerGet.get("/getAllOrdersInDateRange/:uid/:startDate/:endDate", getAllOrdersInDateRange);
routerGet.get("/getAllVehicleTripsInDateRange/:uid/:startDate/:endDate", getAllVehiclesTripsInDateRange);

export default routerGet;
