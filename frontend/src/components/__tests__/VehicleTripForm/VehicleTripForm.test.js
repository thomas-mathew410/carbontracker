import React from 'react';
import { mount } from 'enzyme';

import VehicleTripForm from '../../VehicleTripForm';

describe('Vehicle Trip Form Component', () => {
    it('should render correctly and call onclose function when closed', () => {
        const closeFn = jest.fn();
        const component = mount(<VehicleTripForm uid={999} handleClose={closeFn} />);
        expect(component).toMatchSnapshot();
        const buttons = component.find('button');
        expect(buttons.length).toEqual(2);
        buttons.first().simulate('click');
        expect(closeFn).toHaveBeenCalled();
        component.unmount();

    });
})