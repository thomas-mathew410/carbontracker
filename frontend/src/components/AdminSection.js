import React, {useEffect, useState} from "react";

import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import DeleteDialog from "../components/DeleteDialog";
import { getFirstCharacter } from "../utils/helpers";

const AdminSection = () => {
  const [users, setUsers] = useState([]);
  const [idToDelete, setIdToDelete] = useState(null);
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);

  useEffect(() => {
    fetch(`/api/get/getPeople`)
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setUsers(data);
        }
      });
  }, []);

  const closeDeleteDialog = () => {
    setIdToDelete(null);
    setIsDeleteDialogOpen(false)
  };

  const onDelete = () => {
    fetch(`/api/delete/deletePerson/${idToDelete}`, {
      method: 'DELETE'
    }).then((res) => {
      if (res.ok) {
        console.log('Successfully deleted');
      } else {
        console.log('Unable to delete');
      }
      fetch(`/api/get/getPeople`)
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setUsers(data);
        }
        closeDeleteDialog();
      });
    });
  };



  return (
      <>
        <Grid item>
            <Typography variant="h3" align="center">{"All Registered Users"}</Typography>
        </Grid>
        <Grid item>
          <List>
            <ListItem key="header">
              <ListItemText primary="Users"/>
            </ListItem>
            {users.map((u) => {
                console.log(u);
                const firstLetter = getFirstCharacter(u.username);
              return (<ListItem key={u.uid}>
                <ListItemIcon>
                  <Avatar alt='test'>{firstLetter}</Avatar>
                </ListItemIcon>
                <ListItemText primary={`${u.username}`}/>
                <ListItemSecondaryAction>
                    <IconButton
                        color="secondary"
                        edge="end"
                        onClick={() => {
                        setIdToDelete(u.uid);
                        setIsDeleteDialogOpen(true)
                        }}
                    >
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
              </ListItem>)
            })}
          </List>
        </Grid>
      <DeleteDialog isOpen={isDeleteDialogOpen} onclose={closeDeleteDialog} ondelete={onDelete}></DeleteDialog>
    </>
  );
};

export default AdminSection;
