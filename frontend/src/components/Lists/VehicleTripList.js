import React from 'react';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import DriveEtaIcon from "@material-ui/icons/DriveEta";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Divider from "@material-ui/core/Divider";
import { carbonInfo } from '../../utils/helpers';

const VehicleTripList = ({vehicleTrips, setIdToDelete, setTypeToDelete, setIsDeleteDialogOpen, setIdToEdit, setTypeToEdit, setIsEditDialogOpen}) => {
  return (
    <div>
      <List>
        {vehicleTrips.map((vehicleTrip, i) => {
          const id_vtid = vehicleTrip.vtid;
          const carbonG = vehicleTrip.carbon_g ? vehicleTrip.carbon_g : '-';
          const carbonLb = vehicleTrip.carbon_lb ? vehicleTrip.carbon_lb : '-';
          const carbonKg = vehicleTrip.carbon_kg ? vehicleTrip.carbon_kg : '-';
          const carbonMt = vehicleTrip.carbon_mt ? vehicleTrip.carbon_mt : '-';
          return (
            <div key={id_vtid}>
              <ListItem key={id_vtid} alignItems="flex-start">
                <ListItemIcon>
                  <DriveEtaIcon />
                </ListItemIcon>
                <ListItemText
                  primary={`Total Distance: ${vehicleTrip.distance}${vehicleTrip.distanceunit} on ${vehicleTrip.date_added.split('T')[0]}`}
                  secondary={carbonInfo(carbonG, carbonLb, carbonKg, carbonMt)}
                />
                <ListItemSecondaryAction>
                  <IconButton
                    color="secondary"
                    edge="end"
                    onClick={() => {
                      setIdToDelete(id_vtid);
                      setTypeToDelete('vTrip');
                      setIsDeleteDialogOpen(true)
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                  <IconButton
                    color="secondary"
                    edge="end"
                    onClick={() => {
                      setIdToEdit(id_vtid)
                      setTypeToEdit('vehicleTrips');
                      setIsEditDialogOpen(true)
                    }}
                  >
                    <EditIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
              <Divider variant="fullWidth" component="li" />
            </div>
          );
        })}
      </List>
    </div>
  );
};

export default VehicleTripList;
